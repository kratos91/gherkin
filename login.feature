@MVP @Android @Web @iOS
Feature: Login
  As a customer,I want to login with email and passord,
  So that I can use de app

  Rule: An adult is 18 or older

  Background: 
    Given I am at  the Login page

  Scenario: Logging with valid credentials
    #Given I am at  the Login page
    When I fill the account textbox with value "admin@admin.com"
    And I fill the password textbox with value "123456"
    And I click the login button
    Then I should be at the home page
    And title of home pase is "Global position"
    But login button is not present
    
  Scenario: Logging with invalid credentials
    #Given I am at the Login page
    When I fill the account textbox with value "adminNoValid@admin.com"
    And I fill the password textbox with value "abcdf"
    And I click the login button
    Then a gonna watch a error message with invalid credentials

  Scenario Outline: Logging with valid credentials
    When I fill the account textbox with value <email>
    And I fill the password textbox with value <password>
    And I click the login button
    Then I should be at the home page
    And title of home pase is "Global position"
    But login button is not present

    """ 
    Name: Emiliano Guzmán
    Amount: 10000
    Loans: 10
    """

    Examples: 
    | email            | password | status |
    | admin@admin.com  | 123456   | true   |
    | user@company.com | abcdf    | false  |