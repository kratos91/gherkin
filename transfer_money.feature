@MVP
Feature: Transfer money
  As a bank customer,
  I want to transfer funds between my accounts,
  So that I can fund my creditcard

  Background: 
    Given customer is at the transfer page

  Scenario: Customer has enough money
    #Given customer is at the transfer page 
    Given has enough money
    When customer request transfer money between his accounts
    Then the money is transferred to the second account

  Scenario: Customer don't have enough money
    #Given customer is at the transfer page
    Given hasn't enough money
    When customer request transfer money between his accounts
    Then the money is no transferred to the second account
    And the customer gonna watch a message error

  Scenario Outline: Customer has enough money
    Given has enough money in him <moneyFirstAccount>
    When customer request transfer <money> between his accounts
    Then the money is transferred to <moneySecondAccount>

  Examples:
      | moneyFirstAccount | money | moneySecondAccount | statusSecondAccount |
      | 1000              | 500   | 2500               | 3000                |